﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class RingsControl : MonoBehaviour {

    public int defaultRingsCount = 8;
    public int maxRingsCount = 18;

    public Text ringCountText;
    public Button leftBtn;
    public Button rightBtn;
    public Button solveBtn;
    public Button stopBtn;
    public Text solvedText;

    private int currentRingsCount;
    
    // Use this for initialization
	void Start () {
        currentRingsCount = defaultRingsCount;
        PresentationManager.instance.GenerateDisks(defaultRingsCount);
        ringCountText.text = "" + currentRingsCount;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void IncRings()
    {
        if (currentRingsCount < maxRingsCount)
            PresentationManager.instance.GenerateDisks(++currentRingsCount);
        ringCountText.text = "" + currentRingsCount;
    }

    public void DecRings()
    {
        if (currentRingsCount > 2)
            PresentationManager.instance.GenerateDisks(--currentRingsCount);
        ringCountText.text = "" + currentRingsCount;
    }

    public void EnableUI(bool shouldEnable)
    {
        leftBtn.interactable = shouldEnable;
        rightBtn.interactable = shouldEnable;
        solveBtn.interactable = shouldEnable;
        stopBtn.interactable = !shouldEnable;
    }

    public void Solve()
    {
        if (PresentationManager.instance.state == GameState.DONE)
        {
            PresentationManager.instance.GenerateDisks(currentRingsCount);
        }
        PresentationManager.instance.StartSolving();
        EnableUI(false);
    }

    public void Stop()
    {
        EnableUI(true);

        PresentationManager.instance.BreakSolving();
        PresentationManager.instance.GenerateDisks(currentRingsCount);
    }

    public void Done()
    {
        EnableUI(true);

        StartCoroutine(ShowDoneText());

    }

    IEnumerator ShowDoneText()
    {
        solvedText.gameObject.SetActive(true);
        yield return new WaitForSeconds(3f);
        solvedText.gameObject.SetActive(false);
    }
}
