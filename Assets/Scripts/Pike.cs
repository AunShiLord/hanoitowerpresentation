﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pike : MonoBehaviour
{

    public Transform spawnPoint;
    public Transform topPoint;
    // pike data is stored in Stack (cause FIFO)
    public Stack<GameObject> pikeData = new Stack<GameObject>();
    private float diskHeight = 0.1f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Clear()
    {
        int count = pikeData.Count;
        for (int i = 0; i < count; i++)
        {
            Destroy(pikeData.Pop());
        }
    }

    public GameObject Pop()
    {
        return pikeData.Pop();
    }

    // pushes to Stack and also return position for disk
    public Vector3 Push(GameObject go)
    {
        pikeData.Push(go);
        return spawnPoint.position + new Vector3(0, (pikeData.Count - 1) * diskHeight);
    }

    // generating new disks on this pike
    public void GenerateData(int numberOfDisks, GameObject diskPrefab)
    {
        for (int i = 0; i < numberOfDisks; i++)
        {
            // Instatiating new disk
            GameObject newDisk = Instantiate(diskPrefab, transform.parent);
            newDisk.transform.position = spawnPoint.position + new Vector3(0, diskHeight * i, 0);
            // changing scale of disks
            float scalePct = (numberOfDisks - i) / (float)numberOfDisks;
            newDisk.transform.localScale = new Vector3(diskPrefab.transform.localScale.x * scalePct,
                diskPrefab.transform.localScale.y,
                diskPrefab.transform.localScale.z * scalePct);

            // adding disk to data stack
            pikeData.Push(newDisk);
        }
    }

}
