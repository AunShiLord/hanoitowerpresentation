﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PresentationManager : MonoBehaviour {

    public GameObject diskPrefab;

    // storing data in pikes, that contains Stacks with data (Disks)
    public Pike pikeStart;
    public Pike pikeTemp;
    public Pike pikeEnd;

    public RingsControl ringsControl;

    // singleton instance
    public static PresentationManager instance;

    [HideInInspector]
    public GameState state;

    private void Awake()
    {
        // singleton initialisation
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        state = GameState.NOTHING;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // Clearing all disks from pike 2 and 3 and generating N disks on first pike
    public void GenerateDisks(int numberOfDisks)
    {
        // clearing old pikes
        pikeStart.Clear();
        pikeTemp.Clear();
        pikeEnd.Clear();

        // generating new disks
        pikeStart.GenerateData(numberOfDisks, diskPrefab);

    } 

    public void StartSolving()
    {
        StartCoroutine(MoveDisks(pikeStart, pikeTemp, pikeEnd, pikeStart.pikeData.Count, true));
    }

    // start - from what pike disks are taken
    // end - destination pike
    // temp - temporary tike
    // disks - number of disks
    private IEnumerator MoveDisks(Pike start, Pike temp, Pike end, int disks, bool isFirst = false)
    {
        if (disks > 1)
            yield return MoveDisks(start, end, temp, disks - 1);

        // moving objects from one data to another
        GameObject disk = start.Pop();
        Vector3 finalPath = end.Push(disk);

        // Setting path for animation
        Disk diskComp = disk.GetComponent<Disk>();
        diskComp.AddPath(start.topPoint.position);
        diskComp.AddPath(end.topPoint.position);
        diskComp.AddPath(finalPath);
        ChangeState(GameState.ANIMATING);

        // waiting till the animation is over
        while (state == GameState.ANIMATING)
        {
            yield return null;
        }

        if (disks > 1)
            yield return MoveDisks(temp, start, end, disks - 1);

        // if first of recurcive function is done than solving is done
        if (isFirst)
        {
            ChangeState(GameState.DONE);
            ringsControl.Done();
        }
    }

    // change game state
    public void ChangeState(GameState state)
    {
        this.state = state;
    }

    public void BreakSolving()
    {
        StopAllCoroutines();
    }
    
}
