﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Disk : MonoBehaviour
{
    // list of coordinates that disk must pass throught
    private List<Vector3> movementPath = new List<Vector3>(); 
    private float speed = 8f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (movementPath.Count != 0)
        {
            // moving towards first vector point
            transform.transform.position = Vector3.MoveTowards(transform.transform.position,
                                                                movementPath[0],
                                                                speed * Time.deltaTime);
            if (transform.transform.position == movementPath[0])
            {
                // if at destination point, than removing first vector (second will be first then)
                movementPath.RemoveAt(0);
                if (movementPath.Count == 0)
                {
                    PresentationManager.instance.ChangeState(GameState.SOLVING);
                }
            }
        }
    }

    public void AddPath(Vector3 newPoint)
    {
        movementPath.Add(newPoint);
    }
}
