﻿
// represents current game (presentation) state
public enum GameState
{
    NOTHING,    // doing nothing - ready to solve
    SOLVING,    // solving puzzle
    ANIMATING,  // in process of disk movement animating 
    DONE        // puzzle is solved 
}
